# Flask API "Personas"

== Para ejecución en servidor linux (Google Cloud) ==

_** Se da por entendido que se cuenta con el servidor de base de datos y lo esencial instalado (git, python v3 o mayor, etc.)_

## Se clona el repositorio
## Ingresamos al repositorio
```bash
cd rest-api-python
```

## Crear archivo "config.xml" para manejo de datos de conexión a base de datos.
```xml
    <?xml version="1.0" encoding="UTF-8" ?>
    <configuracion> 
        <host>HOST_DB</host> 
        <user>USUARIO_DB</user> 	
        <database>BASEDATOS_DB</database> 
        <password>CONTRASEÑA_DB</password>
    </configuracion>
```

## Para los scripts en la carpeta src ejecutar en orden (En caso de usar otra base de datos)
- persona.sql
- tpdatospersona.sql
- fnActualizarPersona.sql
- fnBorrarPersona.sql



## Para la conexión con postgresql
```bash
sudo apt-get install postgresql
sudo apt-get install python-psycopg2
sudo apt-get install libpq-dev
```

## En caso de no contar con pip3
```bash
apt install python3-pip
```

## Instalar paqueteria 
```bash
pip3 install -r requeriments.txt
```

## Para ejecución en segundo plano
```bash
python3 app.py & 
```
## Otra opción para ejecución en segundo plano
```bash
nohub python3 app.py
```

## Para ejecución con docker compose
```bash
docker-compose up
```

## Para ejecución con docker compose en segundo plano
```bash
docker-compose up -d
```

### BODY JSON DE SERVICIOS
- POST
```json
{
    "nombre": "NOMBRE",
    "ap_paterno":"PATERNO",
    "ap_materno":"MATERNO(OPCIONAL)",
    "fecha_nac":"YYYY-MM-DD",
    "puesto": "PUESTO"
}
```

- PUT
```json
{
    "nombre": "NOMBRE",
    "ap_paterno":"PATERNO",
    "ap_materno":"MATERNO",
    "fecha_nac":"YYYY-MM-DD",
    "puesto": "PUESTO"
}
```


