FROM python:3.9-alpine

ENV PYTHONUNBUFFERED 1

WORKDIR /app

ENV FLAS_APP app.py

ENV FLASK_RUN_HOST 127.0.0.1

RUN apk add --no-cache postgresql-libs postgresql-dev

RUN apk add libxml2-dev libxslt-dev

RUN apk add --no-cache gcc musl-dev linux-headers

COPY ./requirements.txt /app/requirements.txt

RUN pip install -r requirements.txt

COPY . /app

EXPOSE 3000

CMD [ "flask", "run" ]