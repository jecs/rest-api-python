#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Proyecto: API REST con Flask 
Fecha: 15-10-2020
Autor: Jesús Calderón

Para la simpleza del entendimiento se realizó el ejemplo de la siguiente manera

Conexiones PostgreSQL
    Estan realizadas con "psycopg2" directamente
    Utilizando los Scripts directos en lugar de utilizar funciones

    Para las conexiones también es posible el uso del ORM "sqlalchemy" 

Manejo de Token
    Se utiliza "flask_jwt_simple" ya que es la manera mas rapida de obtención de tokens
    Se limita a solo el uso de un usuario y contraseña en especifico

Datos sensibles
    Los datos sensibles como la conexión a base de datos se maneja en un archivo externo.
    Se decidio por un archivo XML para manejo sencillo por su estructura

    <?xml version="1.0" encoding="UTF-8" ?>
    <configuracion> 
        <host>HOST_DB</host> 
        <user>USUARIO_DB</user> 	
        <database>BASEDATOS_DB</database> 
        <password>CONTRASEÑA_DB</password>
    </configuracion>

Listado Servicios
    http://URL_HOST:5000/obtenerpersonas
        Se obtienen todos los registros existentes
    http://URL_HOST:5000/obtenerpersona/<ID_PERSONA>
        Se obtienen el registro existente correspondiente al ID ingresado
    http://URL_HOST:5000/gettoken
        Se obtiene el token para los servicios que lo requieran
    
    ** REQUIEREN TOKEN **
    http://URL_HOST:5000/borrarPersona/<ID_PERSONA>
        Se borra el registro correspondiente con el ID ingresado
    http://URL_HOST:5000/actualizarpersona/<ID_PERSONA>
        Se actualiza el registro correspondiente con el ID ingresado
    http://URL_HOST:5000/grabarpersona
        Se agrega un nuevo registro a la base de datos
    
'''

from flask import Flask, jsonify, request
from flask_jwt_simple import (
    JWTManager, jwt_required, create_jwt, get_jwt_identity
)
from functools import wraps
import datetime
import psycopg2
from lxml import etree

app = Flask(__name__)

app.config['JWT_SECRET_KEY'] = 'Th1s1sas3cr3t' #Dato que se puede cambiar, solo es para uso de ejemplo

jwt = JWTManager(app)

arrDB=[]
fileLog='' #url para el archivo log

@app.route('/')
def mainapi():
    return jsonify({
        "status":200,
        "message":"Servidor Arriba"
        })

@app.route('/actualizarpersona/<string:id_persona>', methods=['PUT'])
@jwt_required
def actualizarPersona(id_persona):
    if request.json is None:
        return jsonify({
            "status":422,
            "message":"Favor de ingresar datos."
            }), 422

    sNombre = request.json['nombre'].upper()
    sApPaterno = request.json['ap_paterno'].upper()
    sApMaterno = request.json['ap_materno'].upper()
    sFechaNac = request.json['fecha_nac']
    sPuesto = request.json['puesto'].upper()

    if id_persona.isnumeric() == False:
        return jsonify({
            "status":422,
            "message":"Solo se aceptan caracteres numericos."
            }), 422
    
    #Se valida individual para no pasar por alto ninguno
    if sNombre is None:
        sNombre = ""
    if sApPaterno is None:
        sApPaterno = ""
    if sApMaterno is None:
        sApMaterno = ""
    if sFechaNac is None:
        sFechaNac = ""
    if sPuesto is None:
        sPuesto = ""

    if sNombre == "" and sApPaterno == "" and sApMaterno == "" and sFechaNac == "" and sPuesto == "":
        return jsonify({
            "status":422,
            "message":"Favor de ingresar al menos un dato."
            }), 422

    cnx_viva_org = None
    cargarConfiguracion()
    try:
        cnx_viva_org = psycopg2.connect(host=arrDB[0], user=arrDB[1], database=arrDB[2], password=arrDB[3])

        #asigna el cursor a la variable
        actualizarPersonaId = cnx_viva_org.cursor()
		
        sSql = "SELECT keyid, nombre ,paterno, materno, puesto, fecha_nac::char(10) from fnActualizarPersona({0},'{1}','{2}','{3}','{4}','{5}');".format(id_persona, sNombre, sApPaterno, sApMaterno, sFechaNac, sPuesto)
	
        actualizarPersonaId.execute(sSql)
        cnx_viva_org.commit()
         
        persona = None
        for keyid, nombre ,paterno, materno, puesto,fecha_nac in actualizarPersonaId.fetchall(): #para formato JSON
            if keyid is None:
                return jsonify({
                "status":404,
                "message":"No existe persona con ese identificador"
                }), 404
            persona = {
                "id": keyid,
                "fecha_nacimiento": fecha_nac,
                "nombre": nombre,
                "paterno": paterno,
                "materno": materno,
                "puest": puesto,
            }
        
        return jsonify({
            "status":200,
            "message":"Exito!",
            "data": persona
            })

    except (Exception, psycopg2.DatabaseError) as error:
        return jsonify({
            "status":500,
            "message":"Error: %s " % error
            }), 500
    finally:
        if cnx_viva_org is not None:
            cnx_viva_org.close()

@app.route('/borrarPersona/<string:id_persona>', methods=['DELETE'])
@jwt_required
def borrarPersona(id_persona):
    if id_persona.isnumeric() == False:
        return jsonify({
            "status":422,
            "message":"Solo se aceptan caracteres numericos."
            }), 422
    cnx_viva_org = None
    cargarConfiguracion()
    try:
        cnx_viva_org = psycopg2.connect(host=arrDB[0], user=arrDB[1], database=arrDB[2], password=arrDB[3])

        #asigna el cursor a la variable
        borrarPersonaId = cnx_viva_org.cursor()
		
        sSql = "SELECT fnBorrarPersona as resp from fnBorrarPersona({0});".format(id_persona)
	
        borrarPersonaId.execute(sSql)
        cnx_viva_org.commit()
        rows = borrarPersonaId.fetchall()

        resp = [row[0] for row in rows]
        if resp[0] == 1:
            return jsonify({
                "status":200,
                "message":"Exito, registro eliminado de base de datos"
                })
        elif resp[0] == 2:
            return jsonify({
                "status":404,
                "message":"No existe persona con ese identificador"
                }), 404
        else:
            return jsonify({
                "status":500,
                "message":"Ocurrió un error al borrar la persona"
                }), 500

    except (Exception, psycopg2.DatabaseError) as error:
        return jsonify({
            "status":500,
            "message":"Error: %s " % error
            }), 500
    finally:
        if cnx_viva_org is not None:
            cnx_viva_org.close()

@app.route('/grabarpersona', methods=['POST'])
@jwt_required
def grabarPersona():
    if request.json is None:
        return jsonify({
            "status":422,
            "message":"Favor de ingresar datos."
            }), 422

    sNombre = request.json['nombre'].upper()
    sApPaterno = request.json['ap_paterno'].upper()
    sApMaterno = request.json['ap_materno'].upper()
    sFechaNac = request.json['fecha_nac']
    sPuesto = request.json['puesto'].upper()

    if(sNombre == "" or sApPaterno == "" or
    sFechaNac == "" or sPuesto == ""):
        return jsonify({
            "status":422,
            "message":"Parametros incorrectos"
            }), 422
    
    try:
        datetime.datetime.strptime(sFechaNac, '%Y-%m-%d')
        print("Fecha válida")
    except ValueError:
        return jsonify({
            "status":422,
            "message":"Formate de fecha de incorrecto -> YYYY-MM-DD"
            }), 422

    if sApMaterno is None:
        sApMaterno = ""

    cnx_viva_org = None
    cargarConfiguracion()
    try:
        cnx_viva_org = psycopg2.connect(host=arrDB[0], user=arrDB[1], database=arrDB[2], password=arrDB[3])

        #asigna el cursor a la variable
        grabarPersona = cnx_viva_org.cursor()
		
        sSqlRegs = "INSERT INTO persona(nombre, ap_paterno, ap_materno, fecha_nac, puesto) VALUES('{0}','{1}','{2}','{3}','{4}');".format(sNombre,sApPaterno,sApMaterno,sFechaNac,sPuesto)
	
        grabarPersona.execute(sSqlRegs)
        cnx_viva_org.commit()

        return jsonify({
            "status":201,
            "message":"Persona dada de alta correctamente"
            }), 201
    except (Exception, psycopg2.DatabaseError) as error:
        return jsonify({
            "status":500,
            "message":"Error: %s " % error
            }), 500
    finally:
        if cnx_viva_org is not None:
            cnx_viva_org.close()

@app.route('/obtenerpersona/<string:id_persona>')
def obtnerPersona(id_persona):

    if id_persona.isnumeric() == False:
        return jsonify({
            "status":422,
            "message":"Solo se aceptan caracteres numericos."
            }), 422
    cnx_viva_org = None
    cargarConfiguracion()
    try:
        cnx_viva_org = psycopg2.connect(host=arrDB[0], user=arrDB[1], database=arrDB[2], password=arrDB[3])

        #asigna el cursor a la variable
        consultarPersonaId = cnx_viva_org.cursor()
		
        sSql = "SELECT keyid, fecha_nac::char(10), nombre,ap_paterno, ap_materno, puesto FROM persona WHERE stat = 1 AND keyid = {0};".format(id_persona)
	
        consultarPersonaId.execute(sSql)
        cnx_viva_org.commit()

        persona = None
        for keyid, fecha_nac, nombre,ap_paterno, ap_materno, puesto in consultarPersonaId.fetchall(): #para formato JSON
            persona = {
                "id": keyid,
                "fecha_nacimiento": fecha_nac,
                "nombre": nombre,
                "paterno": ap_paterno,
                "materno": ap_materno,
                "puest": puesto,
            }

        if persona is None:
            return jsonify({
            "status":404,
            "message":"No existe persona con ese identificador"
            }), 404

        return jsonify({
            "status":200,
            "message":"Exito!",
            "data": persona
            })

    except (Exception, psycopg2.DatabaseError) as error:
        return jsonify({
            "status":500,
            "message":"Error: %s " % error
            }), 500
    finally:
        if cnx_viva_org is not None:
            cnx_viva_org.close()

@app.route('/obtenerpersonas')
def obtnerPersonas():

    cnx_viva_org = None
    cargarConfiguracion()
    try:
        cnx_viva_org = psycopg2.connect(host=arrDB[0], user=arrDB[1], database=arrDB[2], password=arrDB[3])

        #asigna el cursor a la variable
        consultarPersonas = cnx_viva_org.cursor()
		
        sSql = "SELECT keyid, fecha_nac::char(10), nombre,ap_paterno, ap_materno, puesto FROM persona WHERE stat = 1;"
	
        consultarPersonas.execute(sSql)
        cnx_viva_org.commit()

        personas = []
        for keyid, fecha_nac, nombre,ap_paterno, ap_materno, puesto in consultarPersonas.fetchall():
            persona = {
                "id": keyid,
                "fecha_nacimiento": fecha_nac,
                "nombre": nombre,
                "paterno": ap_paterno,
                "materno": ap_materno,
                "puest": puesto,
            }
            personas.append(persona)

        return jsonify({
            "status":200,
            "message":"Exito!",
            "data": personas
            })

    except (Exception, psycopg2.DatabaseError) as error:
        return jsonify({
            "status":500,
            "message":"Error: %s " % error
            }), 500
    finally:
        if cnx_viva_org is not None:
            cnx_viva_org.close()

@app.route('/gettoken', methods=['POST'])
def obtenerToken():
    if not request.is_json:
        return jsonify({
            "status":400,
            "message":"Favor de ingresar JSON"
            }), 400

    params = request.get_json()
    username = params.get('username', None)
    password = params.get('password', None)

    if not username or not password:
        return jsonify({"message": "Parametro incorrectos"}), 422

    if username != 'user' or password != 'user':
        return jsonify({
            "status":401,
            "message":"Usuario o Contraseña incorrectos"
            }), 401

    cUser = {
        "user":username,
        "pwd": password
    }
    # Identity can be any data that is json serializable
    ret = {"token": create_jwt(identity=cUser)}
    return jsonify({
        "status": 200,
        "message": "Generación exitosa",
        "data": ret
        })

#Funciones genericas
def cargarConfiguracion():
    iElementos=0
    doc = etree.parse("config.xml")
    raiz=doc.getroot()
    iElementos = len(raiz)

    arrDB.append(raiz[0].text)
    arrDB.append(raiz[1].text)
    arrDB.append(raiz[2].text)
    arrDB.append(raiz[3].text)

def escribirLog(sCadena):
	if os.path.exists(fileLog):
		archivo = open (fileLog, 'a')
		archivo.write(time.strftime("%x")+" "+time.strftime('%H:%M:%S')+" : "+sCadena + "\n")
		archivo.close();
	else:
		archivo = open(fileLog, "w")
		archivo.write(time.strftime("%x")+" "+time.strftime('%H:%M:%S')+" : "+sCadena+ "\n")
		archivo.close();

if __name__ == '__main__':
    app.run(debug=False,host='0.0.0.0', port=5000)