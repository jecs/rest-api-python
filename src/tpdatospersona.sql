-- DROP TYPE tpdatospersona CASCADE;

CREATE TYPE tpdatospersona AS (
	keyid integer,    
    nombre VARCHAR(100),
    paterno VARCHAR(50),
	materno VARCHAR(50),
    puesto VARCHAR(100),
	fecha_nac date
);