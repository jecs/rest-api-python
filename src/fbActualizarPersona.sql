CREATE OR REPLACE FUNCTION fnActualizarPersona(integer,character,character,character,character,character)
  RETURNS SETOF tpdatospersona  AS
$BODY$
DECLARE

iIdPersona				ALIAS FOR $1;
sNombre 				ALIAS FOR $2;
sApPaterno				ALIAS FOR $3;
sApMaterno				ALIAS FOR $4;
sFechaNac				ALIAS FOR $5;
sPuesto 				ALIAS FOR $6;

regResp   tpdatospersona;

cNombre         VARCHAR(100);
cApPaterno      VARCHAR(50);
cApMaterno      VARCHAR(50);
cPuesto         VARCHAR(100);
cFechaNac       DATE;

BEGIN
    IF EXISTS(SELECT * FROM persona WHERE keyid = iIdPersona) THEN
        --Se agrega condicional para evitar problemas con la fecha
        IF sFechaNac = '' THEN 
            UPDATE persona SET nombre = sNombre, ap_paterno = sApPaterno, ap_materno = sApMaterno, puesto = sPuesto WHERE keyid = iIdPersona;
        ELSE 
            UPDATE persona SET nombre = sNombre, ap_paterno = sApPaterno, ap_materno = sApMaterno, fecha_nac = sFechaNac::DATE, puesto = sPuesto WHERE keyid = iIdPersona;
        END IF;
        
        SELECT nombre, ap_paterno, ap_materno, puesto, fecha_nac INTO cNombre, cApPaterno, cApMaterno, cPuesto, cFechaNac FROM persona WHERE keyid = iIdPersona order by keyid desc;		
        regResp.keyid = iIdPersona;	
        regResp.nombre = cNombre;
        regResp.paterno = cApPaterno;
        regResp.materno = cApMaterno;
        regResp.puesto = cPuesto;
        regResp.fecha_nac = cFechaNac;
        RETURN NEXT regResp;
        
    ELSE
        RETURN NEXT regResp;
    END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;