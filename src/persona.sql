
DROP TABLE IF EXISTS persona;
CREATE TABLE persona (
  uuid uuid NOT NULL default uuid_generate_v4(),
  keyid SERIAL NOT NULL,
  fecha_alta DATE NOT NULL DEFAULT CURRENT_DATE,
  nombre VARCHAR(100) NOT NULL DEFAULT ''::character varying,
  ap_paterno VARCHAR(50) NOT NULL DEFAULT ''::character varying,
  ap_materno VARCHAR(50) NOT NULL DEFAULT ''::character varying,
  fecha_nac DATE NOT NULL DEFAULT '1900-01-01',
  puesto varchar(100) NOT NULL DEFAULT ''::character varying,
  stat INTEGER NOT NULL DEFAULT 1
);
ALTER TABLE persona OWNER TO postgres;
