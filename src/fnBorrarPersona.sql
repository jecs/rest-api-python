CREATE OR REPLACE FUNCTION fnBorrarPersona(integer)
  RETURNS integer AS
$BODY$

DECLARE

iIdPersona				ALIAS FOR $1;

iResp integer;
BEGIN
    iResp = 0;
    IF EXISTS(SELECT * FROM persona WHERE keyid = iIdPersona) THEN
        DELETE FROM persona WHERE keyid = iIdPersona;
        iResp = 1;
    ELSE
			iResp = 2;
    END IF;

   RETURN (iResp);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;